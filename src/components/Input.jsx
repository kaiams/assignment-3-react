import './input.css'

//form with input and button
export const Input = (props) => {
    return(
        <form className={`form ${props.className}`} onSubmit={props.onSubmit}>
            |
            <input placeholder={props.placeholder} className="form-input" onChange={props.onChange}/>
            <button className="form-button" type="submit">-{'->'}</button>
        </form>
    )
    

}