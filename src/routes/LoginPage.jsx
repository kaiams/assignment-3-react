import { Redirect } from 'react-router-dom';
import { useState } from 'react';
import { RobotImg } from '../components/RobotImg';
import { Input } from '../components/Input';
import './login-page.css';
import { useDispatch, useSelector } from 'react-redux';
import { loginAttemptAction } from '../store/actions/loginAction';


export const LoginPage = () => {
    const dispatch = useDispatch();
    const [user, setUser] = useState({
         name: "", translations: [] 
    });
    const { loginError, loginAttempt } = useSelector(state => state.loginReducer);
    const { loggedIn} = useSelector(state => state.sessionReducer)
   


    const handleUserName = (event) => {
        //Stop page reload.
        event.preventDefault();
        if(user.name !== ""){
            dispatch(loginAttemptAction(user))
        }
    }
   
    return(
        <>
        {/* Redirect to translate page if logged in  */}
        {loggedIn && <Redirect to="/translate"/>}
        {!loggedIn && 
            <div>
            <RobotImg className="robot-img"/>
            <span>
                <h1>Lost in Translation</h1>
                <h4>Get started</h4>
            </span>
            
            <div className="login-input-div">
                <Input placeholder="What's your name?" onSubmit={event => handleUserName(event)} onChange={(event => {
                    setUser({...user, name : event.target.value})
                })}/>
            </div>
            { loginAttempt && 
            <h4>Trying to log in</h4>}
            { loginError && 
            <h4>Login unsuccsessful {loginError}</h4>
            }
            </div>
        }
        </>
    );
}