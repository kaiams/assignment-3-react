export const ACTION_TRANSLATE_ATTEMPT = "[translate] ATTEMPT";
export const ACTION_TRANSLATE_SUCCESS = "[translate] SUCCESS";
export const ACTION_TRANSLATE_ERROR = "[translate] ERROR";

export const translateAttemptAction = (translate) => ({
  type: ACTION_TRANSLATE_ATTEMPT,
  payload: translate,
});

export const translateSuccessAction = (translations) => ({
  type: ACTION_TRANSLATE_SUCCESS,
  payload: translations,
});

export const translateErrorAction = (error) => ({
  type: ACTION_TRANSLATE_ERROR,
  payload: error,
});
