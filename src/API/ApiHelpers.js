const url = "http://localhost:3000/profiles";
export const ApiHelpers = {
  getUser(user) {
    return fetch(`${url}?name=${user.name}`, {
      method: "GET",
      headers: { "Content-Type": "apllication/json" },
    }).then(async (response) => {
      if (!response.ok) {
        const { error = "An unknown error occurred" } = await response.json();
        throw new Error(error);
      }
      return response.json();
    });
  },
  addUser(user) {
    return fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(user),
    }).then(async (response) => {
      if (!response.ok) {
        const { error = "An unknown error occurred" } = await response.json();
        throw new Error(error);
      }
      return response.json();
    });
  },

  //Patch existing object to update translations array
  UpdateTranslate(id, translations) {
    return fetch(`${url}/${id}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ translations: translations }),
    }).then(async (response) => {
      if (!response.ok) {
        const { error = "An unknown error occurred" } = await response.json();
        throw new Error(error);
      }
      return response.json();
    });
  },
};
