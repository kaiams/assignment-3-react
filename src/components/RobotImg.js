import "./robot.css";

//The robot image is placed in front of the Splash.
export const RobotImg = () => {
  return (
    <div className="img-container">
      <img
        className="splash"
        src="/LostInTranslation_Resources/Splash.svg"
        alt="splash"
      />
      <img
        className="robot"
        src="/LostInTranslation_Resources/Logo.png"
        alt="smiling robot"
      />
    </div>
  );
};
