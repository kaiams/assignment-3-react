import { useState } from 'react';
import { Input } from '../components/Input';
import { useDispatch, useSelector } from 'react-redux';
import './translation-page.css'
import { Redirect } from 'react-router-dom';
import { translateAttemptAction } from '../store/actions/translateAction';

export const TranslationPage = () => {
    const[toBeTranslated, setToBeTranslated] = useState("");
    const[translationImgs, setTranslationImgs] = useState([]);
    const dispatch = useDispatch();
    const { loggedIn, id, translations } = useSelector(state => state.sessionReducer)

    //Function that ads word that was translated to the translations array.
    //If there are 10 translations in the array, the first/oldest element is removed.
    const addWordToTranslations = () =>{
        if(translations.length === 10){
            translations.shift();
        }
        translations.push(toBeTranslated);
        dispatch(translateAttemptAction({id: id, translations: translations}));
    }
    //Function to add input it form to translate if it is not an empty string. 
    const translate = (event) => {
        event.preventDefault();
        if(toBeTranslated !== ""){
            addWordToTranslations();
            const onlyLetters = toBeTranslated.split("").filter((letter) => letter.match(/[a-zA-Z]/))
            const imgArray = onlyLetters.map((letter) => {
                return `/lostInTranslation_Resources/individual_signs/${letter.toLowerCase()}.png`
            })
            setTranslationImgs(imgArray);
        }
    }
    return(
        <div>
            {!loggedIn && <Redirect to="/"/>}
            <Input className="input-translate" placeholder="Translate " onSubmit={event => translate(event)} 
            onChange={(event => setToBeTranslated(event.target.value))}/>

            <div className="div-translation">

                {translationImgs.map((imgSrc, i) =>
                     <img className="sign-img" key={i} alt={imgSrc} src={imgSrc}/>
                    )}
                <div className="translation-description">
                    <p className="description-pill">Translation</p>
                </div>
            </div>
        </div>
    );
}