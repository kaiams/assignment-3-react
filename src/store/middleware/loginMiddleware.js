import { ApiHelpers } from "../../API/ApiHelpers";
import {
  ACTION_LOGIN_ATEMPT,
  ACTION_LOGIN_SUCCESS,
  loginSuccessAction,
  loginErrorAction,
} from "../actions/loginAction";
import { sessionSetAction } from "../actions/sessionAction";

export const loginMiddleware =
  ({ dispatch }) =>
  (next) =>
  (action) => {
    next(action);

    if (action.type === ACTION_LOGIN_ATEMPT) {
      //Get request to check if user exists. If not, new user is added
      ApiHelpers.getUser(action.payload)
        .then((profile) => {
          profile.length === 0
            ? ApiHelpers.addUser(action.payload)
                .then((profiles) => {
                  dispatch(loginSuccessAction(profiles));
                })
                .catch((error) => {
                  dispatch(loginErrorAction(error.message));
                })
            : dispatch(loginSuccessAction(profile[0]));
        })
        .catch((error) => {
          dispatch(loginErrorAction(error.message));
        });
    }

    if (action.type === ACTION_LOGIN_SUCCESS) {
      dispatch(sessionSetAction(action.payload));
    }
  };
