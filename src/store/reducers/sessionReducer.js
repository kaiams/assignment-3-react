import {
  ACTION_SESSION_SET,
  ACTION_SESSION_UPDATE,
} from "../actions/sessionAction";

const initialState = {
  name: "",
  id: "",
  translations: [],
  loggedIn: false,
};

export const sessionReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_SESSION_SET:
      return {
        ...action.payload,
        loggedIn: true,
      };

    case ACTION_SESSION_UPDATE:
      return {
        ...action.payload,
        loggedIn: true,
      };
    default:
      return state;
  }
};
