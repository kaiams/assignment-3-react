import {
  ACTION_TRANSLATE_ATTEMPT,
  ACTION_TRANSLATE_ERROR,
  ACTION_TRANSLATE_SUCCESS,
} from "../actions/translateAction";

const initialState = {
  addTranslateAttempt: false,
  addTranslateError: "",
};

export const translateReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_TRANSLATE_ATTEMPT:
      return {
        ...state,
        addTranslateAttempt: true,
        addTranslateError: "",
      };
    case ACTION_TRANSLATE_SUCCESS:
      return {
        ...initialState,
      };
    case ACTION_TRANSLATE_ERROR:
      return {
        ...state,
        addTranslateAttempt: false,
        addTranslateError: action.payload,
      };
    default:
      return state;
  }
};
