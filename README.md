# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Description

This program translates letters into sign language.
One has to log in by entering a userName. Every word or sentance one wants to translate will be stored in the user. The profile page displays the last 10 words that have been translated. One can delete the whole translations log. This will set every instance in the array to 'deleted'.

## JSON databasefile

Run the JSON file 'profiles.json' on localhost 3030

## Extra information to Dewald :D

I ran a bit short on time so the styling isn't exactly perfect. I do have an issue: when i translate something it's added to the API and will show when going to the profile tab. If one refreshes the page the newly added translations will dissapear even when i can see them in the JSON file. Maybe i don't get or set the response i think i do back after patching.

I also only have the last 10 translations in the api at all times.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.
