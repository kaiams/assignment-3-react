import { ApiHelpers } from "../../API/ApiHelpers";
import {
  ACTION_TRANSLATE_ATTEMPT,
  ACTION_TRANSLATE_SUCCESS,
  translateErrorAction,
  translateSuccessAction,
} from "../actions/translateAction";

import { sessionUpdateAction } from "../actions/sessionAction";

export const translateMiddleware =
  ({ dispatch }) =>
  (next) =>
  (action) => {
    next(action);

    if (action.type === ACTION_TRANSLATE_ATTEMPT) {
      ApiHelpers.UpdateTranslate(action.payload.id, action.payload.translations)
        .then((translations) => dispatch(translateSuccessAction(translations)))
        .catch((error) => dispatch(translateErrorAction(error.message)));
    }

    if (action.type === ACTION_TRANSLATE_SUCCESS) {
      dispatch(sessionUpdateAction(action.payload));
    }
  };
