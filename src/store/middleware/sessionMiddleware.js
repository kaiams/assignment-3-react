import {
  ACTION_SESSION_INIT,
  ACTION_SESSION_SET,
  ACTION_SESSION_UPDATE,
  sessionSetAction,
} from "../actions/sessionAction";

export const sessionMiddleware =
  ({ dispatch }) =>
  (next) =>
  (action) => {
    next(action);

    if (action.type === ACTION_SESSION_INIT) {
      const storedSession = localStorage.getItem("rtxtf-ss");
      if (storedSession) {
        const session = JSON.parse(storedSession);
        dispatch(sessionSetAction(session));
      }
    }
    if (action.type === ACTION_SESSION_SET) {
      //store profile in local storage;
      localStorage.setItem("rtxtf-ss", JSON.stringify(action.payload));
    }

    if (action.typ === ACTION_SESSION_UPDATE) {
      localStorage.setItem("rtxtf-ss", JSON.stringify(action.payload));
    }
  };
