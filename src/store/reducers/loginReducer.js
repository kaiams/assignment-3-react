import {
  ACTION_LOGIN_ATEMPT,
  ACTION_LOGIN_ERROR,
  ACTION_LOGIN_SUCCESS,
} from "../actions/loginAction";

const initialState = {
  loginAttempt: false,
  loginError: "",
};

export const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_LOGIN_ATEMPT:
      return {
        ...state,
        loginAttempt: true,
        loginError: "",
      };
    case ACTION_LOGIN_SUCCESS:
      return {
        ...initialState,
      };
    case ACTION_LOGIN_ERROR:
      return {
        ...state,
        loginAttempt: false,
        loginError: action.payload,
      };
    default:
      return state;
  }
};
