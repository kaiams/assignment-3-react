import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { RobotImg } from "./RobotImg";
import './header.css'


export const Header = () => {
    const { name, loggedIn} = useSelector(state => state.sessionReducer)
    let history = useHistory();

    //Header element.
    //Displays img and profile button when one is logged in and the apps title at all times.
    return(
        <div>
            <header className="header">
                {loggedIn?
                    <>
                        <RobotImg className="robot-img"/> 
                        <p>Lost in Translation</p>
                        <div>
                            <button className="profile" onClick={() => {history.push("/translate/profile")}}>{name}</button>
                        </div>
                    </>
                    : <p>Lost in Translation</p>
                }
            </header>
            <hr/>
         </div>
            
    )
}