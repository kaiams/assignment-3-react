import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import "./App.css";
import { LoginPage } from "./routes/LoginPage";
import { ProfilePage } from "./routes/ProfilePage";
import { TranslationPage } from "./routes/TranslationPage";
import { Header } from "./components/Header";
function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Header />
        <Switch>
          <Route path="/" exact>
            <Redirect to="/login" />
          </Route>
          <Route path="/login" component={LoginPage} />
          <Route path="/translate" exact component={TranslationPage} />
          <Route path="/translate/profile" component={ProfilePage} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
