import { useDispatch, useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { translateAttemptAction } from '../store/actions/translateAction';
import './profile-page.css'

export const ProfilePage = () =>{
    //states with information on this session
    const { name, id, loggedIn, translations} = useSelector(state => state.sessionReducer);
    const dispatch = useDispatch();

    //Function to mark the entire translations array as deleted.
    const markAllAsDeleted = () => {
        const deletedArray = translations.map( (translation) => 
            translation = "deleted"
        )
        dispatch(translateAttemptAction({id: id, translations: deletedArray}))
    }
    return (
        <div>
            {!loggedIn && <Redirect to="/"/>}
            <p>Welcome {name}</p>
            <button onClick={markAllAsDeleted}>Clear all translations</button>
            <p>Your {translations.filter((translation) => translation !== "deleted").length} last translations</p>

            {/* Shows the last 10 translations (filters away all the deleted) */}
            <div className="all-translations">
                {translations.filter((translation) => translation !== "deleted").map( (translation, id) => 
                    <div key={id}>
                        {translation}
                    </div>)}
            </div>

                {/* Logging out, clears the local storage and reloads the page to be redirected to login  */}
           
                <button onClick={() => {localStorage.clear(); window.location.reload()}}>Log out</button>
    
        </div>
    );
}