export const ACTION_LOGIN_ATEMPT = "[login] ATEMPT";
export const ACTION_LOGIN_SUCCESS = "[login] SUCCESS";
export const ACTION_LOGIN_ERROR = "[login] ERROR";

export const loginAttemptAction = (userName) => ({
  type: ACTION_LOGIN_ATEMPT,
  payload: userName,
});

export const loginSuccessAction = (profile) => ({
  type: ACTION_LOGIN_SUCCESS,
  payload: profile,
});

export const loginErrorAction = (error) => ({
  type: ACTION_LOGIN_ERROR,
  payload: error,
});
